package log530.tp3.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;

import log530.tp3.R;
import log530.tp3.gl.Constants;
import log530.tp3.gl.GameThread;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private GameThread mGameThread;

    public GameView(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);


        final SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        mGameThread = new GameThread(surfaceHolder, context, attrs);
        setFocusable(true);
    }

    public void setThreadUIHandler(Handler handler)
    {
        mGameThread.setUIHandler(handler);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        if (mGameThread.isInBackground()) {
            mGameThread.setInBackground(false);
        }
        else
        {
            mGameThread.setRunning();
            mGameThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mGameThread.setSurfaceSize(width, height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mGameThread.setInBackground(true);
    }

    public void moveBlock(Constants.PossibleMoves possibleMove) {
        mGameThread.moveBlock(possibleMove);
    }

    public void rotateBlock() {
        mGameThread.rotateBlock();
    }

    public void instantDropBlock() {
        mGameThread.instantDropBlock();
    }
}
