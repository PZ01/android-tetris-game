package log530.tp3.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import log530.tp3.R;
import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;
import log530.tp3.gl.GridModel;
import log530.tp3.util.PaintManager;
import log530.tp3.util.RessourceManager;

public class GridView extends View {

    private RessourceManager resManager;
    private PaintManager paintManager;
    private RectF mBounds;
    private int mCellWidthUI;
    private int mCellHeightUI;
    private Bitmap mBackgroundImage;
    private GridModel mGridModel;

    private Bitmap[] mBlockBitmaps = new Bitmap[8];

    public GridView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);

        float boardPadding = 0;

        // Unpack xml attributes
        TypedArray typedArr = ctx.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.GameView,
                0, 0);

        try {
            boardPadding = typedArr.getDimension(R.styleable.GameView_gridPadding, 10.0f);
        }
        finally {
            typedArr.recycle();
        }

        resManager = RessourceManager.getInstance(ctx);
        paintManager = PaintManager.getInstance(ctx.getResources());
        mGridModel = new GridModel(ctx, attrs);
        mBounds = new RectF(boardPadding, boardPadding, 0.0f, 0.0f);
    }

    //PZ::TODO might want to experiment with onSizeChanged method override
    public void resize(float width, float height) {
        mBounds.right = width - mBounds.left;
        mBounds.bottom = height - mBounds.top;
        mCellWidthUI  = Math.round((mBounds.right - mBounds.left) / mGridModel.getHorizontalCellCnt());
        mCellHeightUI = Math.round((mBounds.bottom - mBounds.top) / mGridModel.getVerticalCellCnt());

        createBlockBitmaps(mCellWidthUI, mCellHeightUI);

        mBackgroundImage = createdScaledBgGrid(width, height);
    }

    private void createBlockBitmaps(int cellWidth, int cellHeight) {
        Context ctx = getContext();
        for (int i=0; i<8; ++i) {
            mBlockBitmaps[i] = Bitmap.createScaledBitmap(
                    Block.getBitmapForBlock(ctx, Constants.GameShapes.values()[i]),
                    cellWidth,
                    cellHeight,
                    false
            );
        }
    }

    private Bitmap createdScaledBgGrid(float viewWidth, float viewHeight)
    {
        int desiredWidth = (int)  (viewWidth - (mBounds.left * 2));
        int desiredHeight = (int) (viewHeight - (mBounds.top * 2));
        mBackgroundImage = Bitmap.createScaledBitmap(resManager.StrippedBg(),
                desiredWidth, desiredHeight, false);

        return mBackgroundImage;
    }

    public int getCellWidthUI() {
        return mCellWidthUI;
    }

    public int getCellHeightUI() {
        return mCellHeightUI;
    }

    @Deprecated
    public GridModel gridModel()
    {
       return mGridModel;
    }

    @Override
    public void draw(Canvas canvas) {

        // Sometimes resize will be called after draw, which would mean we have no background
        if (mBackgroundImage == null)
           mBackgroundImage = createdScaledBgGrid(canvas.getWidth(), canvas.getHeight());

        canvas.drawBitmap(mBackgroundImage, mBounds.left, mBounds.top, null);

        Paint p = paintManager.ThickGrayStroke();

        canvas.drawLine(mBounds.left, mBounds.top, mBounds.left, mBounds.bottom, p);
        canvas.drawLine(mBounds.right, mBounds.top, mBounds.right, mBounds.bottom, p);
        canvas.drawLine(mBounds.left, mBounds.bottom, mBounds.right, mBounds.bottom, p);

        drawBlocks(canvas);
    }

    public RectF bounds()
    {
        return mBounds;
    }

    Constants.GameShapes convertFromInt(int shapeAsInt)
    {
        switch (shapeAsInt)
        {
            case 0:
                return Constants.GameShapes.EMPTY;
            case 1:
                return Constants.GameShapes.I;
            case 2:
                return Constants.GameShapes.J;
            case 3:
                return Constants.GameShapes.L;
            case 4:
                return Constants.GameShapes.O;
            case 5:
                return Constants.GameShapes.S;
            case 6:
                return Constants.GameShapes.T;
            case 7:
                return Constants.GameShapes.Z;
        }

        return null;
    }

    @Deprecated
    public void drawBlocks(Canvas canvas)
    {
        int [][] grid = mGridModel.grid();
        for(int row = 0 ; row < grid.length ; ++row)
        {
            for (int col = 0 ; col < grid[row].length ; ++col)
            {
                if (grid[row][col] != Constants.GameShapes.EMPTY.val())
                {
                    Constants.GameShapes gameShape = convertFromInt(grid[row][col]);
                    int top = (int) ((row * mCellHeightUI) + mBounds.top);
                    int left = (int) ((col * mCellWidthUI) + mBounds.left);
                    canvas.drawBitmap(mBlockBitmaps[grid[row][col]], left, top, null);
                }
            }
        }
    }

    private int boardWidth()
    {
        return (int) (mBounds.right - mBounds.left);
    }

    private int boardHeight()
    {
        return (int) (mBounds.bottom - mBounds.top);
    }
}
