package log530.tp3;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import log530.tp3.gl.Constants;
import log530.tp3.ui.GameView;
import log530.tp3.util.SoundManager;

public class GameActivity extends FragmentActivity {

    private GameView mGameView;
    private ImageButton mRotationBtn;
    private ImageButton mLeftBtn;
    private ImageButton mDownBtn;
    private ImageButton mRightBtn;
    private Handler mHandler;
    public static final String VISIBILITY_KEY = "visibility";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mGameView = (GameView)findViewById(R.id.gameView);
        mRotationBtn = (ImageButton) findViewById(R.id.rotateCtrlBtn);
        mLeftBtn = (ImageButton) findViewById(R.id.leftCtrlBtn);
        mDownBtn = (ImageButton) findViewById(R.id.middleCtrlBtn);
        mRightBtn = (ImageButton) findViewById(R.id.rightCtrlBtn);

        mHandler = new Handler()
                {
                    @Override
                    public void handleMessage(Message msg) {
                        int vis = msg.getData().getInt(VISIBILITY_KEY);
                        mRotationBtn.setVisibility(vis);
                        mLeftBtn.setVisibility(vis);
                        mDownBtn.setVisibility(vis);
                        mRightBtn.setVisibility(vis);
                    }
                };
        SoundManager.initBackgroundMusic(this);
        mGameView.setThreadUIHandler(mHandler);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void ctrlLeftPressed(View view)
    {
       mGameView.moveBlock(Constants.PossibleMoves.LEFT);
    }

    public void ctrlMiddlePressed(View view)
    {
        mGameView.moveBlock(Constants.PossibleMoves.DOWN);
    }

    public void ctrlRightPressed(View view)
    {
        mGameView.moveBlock(Constants.PossibleMoves.RIGHT);
    }

    public void ctrlRotatePressed(View view)
    {
        mGameView.rotateBlock();
    }

    public void ctrlInstantDropDownPressed(View view) { mGameView.instantDropBlock(); }
}
