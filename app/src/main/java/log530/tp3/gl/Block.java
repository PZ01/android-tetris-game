package log530.tp3.gl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

import log530.tp3.R;
import log530.tp3.util.RessourceManager;

public abstract class Block {

    //Top-left position of the block in the board
    protected int mPosX;
    protected int mPosY;

    //Top-left position of the block for the next val
    protected int mPotentialX;
    protected int mPotentialY;

    protected Constants.GameShapes mType;

    protected int mShapeWidth;
    protected int mShapeHeight;

    //Rotation cycle
    protected int mRotationCycle;

    protected Constants.GameShapes[][] mBlockGrid;
    protected Constants.GameShapes[][] mPotentialBlockGrid;

    private Bitmap mBlockBitmap;

    //PZ::TODO These two ints are the scaled size of a single block, their names will eventually change
    private int mScaledWidthUI;
    private int mScaledHeightUI;

    /**
     * Base Block constructor
     *
     * @param type        the underlying shape type(L-block, Square-block, etc...)
     * @param posX        the horizontal axis spawning position
     * @param shapeWidth  the number of cells in width this shape occupies
     * @param shapeHeight the number of cells in height this shape occupies
     */
    protected Block(Context ctx, Constants.GameShapes type, int posX, int shapeWidth, int shapeHeight) {
        mType = type;
        mPosX = posX;
        mPotentialX = posX;
        mPosY = 0;
        mPotentialY = 0;
        mShapeWidth = shapeWidth;
        mShapeHeight = shapeHeight;
        adjustOutOfBoundsSpawn(ctx, posX);
        mBlockGrid = new Constants.GameShapes[shapeHeight][shapeWidth];
        mPotentialBlockGrid = new Constants.GameShapes[shapeHeight][shapeWidth];
        clearGrid(mBlockGrid);
        clearGrid(mPotentialBlockGrid);
        mRotationCycle = 0;

        mBlockBitmap = getBitmapForBlock(ctx, type);
    }

    public static Bitmap getBitmapForBlock(Context ctx, Constants.GameShapes type) {
        if (type.equals(Constants.GameShapes.EMPTY)) {
            return Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
        }
        Bitmap mSrcBitmap = RessourceManager.getInstance(ctx).BlockSprite();

        int blockSize = mSrcBitmap.getHeight();
        int posXPx = blockSize * (type.val() - 1);
        return Bitmap.createBitmap(mSrcBitmap, posXPx, 0, blockSize, blockSize);
    }

    protected Constants.GameShapes[][] create2dArray(int rows, int cols) {
        mShapeHeight = rows;
        mShapeWidth = cols;
        Constants.GameShapes[][] tmpGrid = new Constants.GameShapes[rows][cols];
        clearGrid(tmpGrid);

        return tmpGrid;
    }

    public void resize(int cellWidthUI, int cellHeightUI) {
        mScaledWidthUI =  cellWidthUI;
        mScaledHeightUI = cellHeightUI;

        mBlockBitmap = Bitmap.createScaledBitmap(mBlockBitmap,
                mScaledWidthUI, mScaledHeightUI, false);
    }

    private void adjustOutOfBoundsSpawn(Context ctx, int posX) {

        int gridHorizontalWidth = ctx.getResources().getInteger(R.integer.grid_horizontal_cell_cnt);
        if((mPosX + mShapeWidth) >= gridHorizontalWidth)
        {
            mPosX -= mShapeWidth;
            mPotentialX -= mShapeWidth;
        }
    }

    public void potentialMove(final Constants.PossibleMoves possibleMove, final int gridHorizontalCellCnt) {
        if (possibleMove == Constants.PossibleMoves.DOWN) {
            mPotentialY++;
        } else if (possibleMove == Constants.PossibleMoves.UP) {
            mPotentialY--;
        } else {
            if ((mPotentialX + possibleMove.val()) < 0 || (mPotentialX + possibleMove.val()) >= gridHorizontalCellCnt)
                return;

            mPotentialX += possibleMove.val();
        }
    }

    public void move(final Constants.PossibleMoves possibleMove, final int gridHorizontalCellCnt) {
        if (possibleMove == Constants.PossibleMoves.DOWN || possibleMove == Constants.PossibleMoves.UP) {
            mPosY = mPotentialY;
        } else {
            if (mPotentialX >= 0 || mPotentialX + possibleMove.val() < gridHorizontalCellCnt)
                mPosX = mPotentialX;
        }
    }

    protected void clearGrid(Constants.GameShapes blockGrid[][])
    {
        for (int i = 0; i < blockGrid.length ; i++)
        {
            for (int j = 0; j < blockGrid[i].length; j++)
            {
                blockGrid[i][j] = Constants.GameShapes.EMPTY;
            }
        }
    }

    public void draw(Canvas canvas, RectF bounds) {

        for (int row = 0; row < mBlockGrid.length; row++) {
            for (int col = 0; col < mBlockGrid[row].length; col++) {
                if (mBlockGrid[row][col] != Constants.GameShapes.EMPTY) {
                    float posX = (col + mPosX) * mScaledWidthUI;
                    float posY = (row + mPosY) * mScaledHeightUI;
                    canvas.drawBitmap(mBlockBitmap, posX + bounds.left, posY + bounds.top, null);
                }
            }
        }
    }

    //PZ::TODO This name isn't really inlined with the Constants.GameShapes
    //equivalent
    public Constants.GameShapes type() {
        return mType;
    }

    public int shapeHeight() {
        return mShapeHeight;
    }

    public int shapeWidth() {
        return mShapeWidth;
    }

    //PZ::TODO This method could use some renaming, or the variables
    public int potentialX() {
        return mPotentialX;
    }

    //PZ::TODO This method could use some renaming, or the variables
    public int potentialY() {
        return mPotentialY;
    }

    //PZ::TODO This method could use some renaming, or the variables
    public int row() {
        return mPosY;
    }

    //PZ::TODO This method could use some renaming, or the variables
    public int col() {
        return mPosX;
    }

    public boolean isNull() { return false; }
    public Constants.GameShapes blockAt(int row, int col) {
        return mBlockGrid[row][col];
    }

    public void decreaseRotationCycle() {
        mRotationCycle--;
    }

    public abstract void potentialRotate();

    public void rotate()
    {
        mBlockGrid = mPotentialBlockGrid;
    }
}
