package log530.tp3.gl.sm;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;

/**
 * Created by pz on 17/07/15.
 */
public class EmptyBlock extends Block {

    private final static int EMPTY_CELL_WIDTH = 0;
    private final static int EMPTY_CELL_HEIGHT = 0;

    public EmptyBlock(Context ctx) {
        super(ctx, Constants.GameShapes.EMPTY, 0, EMPTY_CELL_WIDTH, EMPTY_CELL_HEIGHT);
    }

    @Override
    public boolean isNull()
    {
        return true;
    }

    @Override
    public void draw(Canvas canvas, RectF bounds)
    {

    }

    @Override
    public void rotate()
    {

    }

    @Override
    public void potentialRotate() {

    }
}
