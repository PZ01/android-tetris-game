package log530.tp3.gl.sm;

import android.content.Context;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;

public class OBlock extends Block {

    private final static int O_CELL_CNT = 2;

    /**
     * Base Block constructor
     *
     * @param type        the underlying shape type(L-block, Square-block, etc...)
     * @param posX        the horizontal axis spawning position
     */
    public OBlock(Context ctx, Constants.GameShapes type, int posX) {
        super(ctx, type, posX, O_CELL_CNT, O_CELL_CNT);

        //[row][col]
        mBlockGrid[0][0] = Constants.GameShapes.O;
        mBlockGrid[0][1] = Constants.GameShapes.O;
        mBlockGrid[1][0] = Constants.GameShapes.O;
        mBlockGrid[1][1] = Constants.GameShapes.O;
    }

    @Override
    public void potentialRotate() {

    }

    @Override
    public void rotate()
    {

    }
}
