package log530.tp3.gl.sm;

import android.content.Context;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;

/**
 * Created by root on 7/10/15.
 */
public class SBlock extends Block {

    private final static int S_CELL_WIDTH = 3;
    private final static int S_CELL_HEIGHT = 2;

    /**
     * Base Block constructor
     *
     * @param type        the underlying shape type(L-block, Square-block, etc...)
     * @param posX        the horizontal axis spawning position
     */
    public SBlock(Context ctx, Constants.GameShapes type, int posX) {
        super(ctx, type, posX, S_CELL_WIDTH, S_CELL_HEIGHT);

        //[row][col]
        mBlockGrid[0][0] = Constants.GameShapes.EMPTY;
        mBlockGrid[0][1] = Constants.GameShapes.S;
        mBlockGrid[0][2] = Constants.GameShapes.S;

        mBlockGrid[1][0] = Constants.GameShapes.S;
        mBlockGrid[1][1] = Constants.GameShapes.S;
        mBlockGrid[1][2] = Constants.GameShapes.EMPTY;
    }

    @Override
    public void potentialRotate() {

        if(mRotationCycle == 0)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(3, 2);

            //|L|0|
            //|L|L|
            //|0|L|
            mPotentialBlockGrid[0][0] = Constants.GameShapes.S;
            mPotentialBlockGrid[1][0] = Constants.GameShapes.S;
            mPotentialBlockGrid[1][1] = Constants.GameShapes.S;
            mPotentialBlockGrid[2][1] = Constants.GameShapes.S;
        }
        else if(mRotationCycle == 1)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(2, 3);

            //|0|L|L|
            //|L|L|0|
            mPotentialBlockGrid[0][1] = Constants.GameShapes.S;
            mPotentialBlockGrid[0][2] = Constants.GameShapes.S;
            mPotentialBlockGrid[1][0] = Constants.GameShapes.S;
            mPotentialBlockGrid[1][1] = Constants.GameShapes.S;
        }


        if(mRotationCycle++ == 1)
            mRotationCycle = 0;
    }
}
