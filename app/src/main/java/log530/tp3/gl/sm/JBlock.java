package log530.tp3.gl.sm;

import android.content.Context;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;

/**
 * Created by david on 7/10/15.
 */
public class JBlock extends Block {

    private final static int J_CELL_WIDTH = 2;
    private final static int J_CELL_HEIGHT = 3;

    /**
     * Base Block constructor
     *
     * @param type        the underlying shape type(L-block, Square-block, etc...)
     * @param posX        the horizontal axis spawning position
     */
    public JBlock(Context ctx, Constants.GameShapes type, int posX) {
        super(ctx, type, posX, J_CELL_WIDTH, J_CELL_HEIGHT);

        //[row][col]
        mBlockGrid[0][1] = Constants.GameShapes.L;
        mBlockGrid[0][0] = Constants.GameShapes.EMPTY;

        mBlockGrid[1][1] = Constants.GameShapes.L;
        mBlockGrid[1][0] = Constants.GameShapes.EMPTY;

        mBlockGrid[2][0] = Constants.GameShapes.L;
        mBlockGrid[2][1] = Constants.GameShapes.L;
    }

    @Override
    public void potentialRotate() {

        if(mRotationCycle == 0)
        {
            //clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(2, 3);

            //|L|0|0|
            //|L|L|L|
            mPotentialBlockGrid[0][0] = Constants.GameShapes.J;
            mPotentialBlockGrid[1][0] = Constants.GameShapes.J;
            mPotentialBlockGrid[1][1] = Constants.GameShapes.J;
            mPotentialBlockGrid[1][2] = Constants.GameShapes.J;
        }
        else if(mRotationCycle == 1)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(3, 2);

            //|L|L|
            //|L|0|
            //|L|0|
            mPotentialBlockGrid[0][0] = Constants.GameShapes.J;
            mPotentialBlockGrid[0][1] = Constants.GameShapes.J;
            mPotentialBlockGrid[1][0] = Constants.GameShapes.J;
            mPotentialBlockGrid[2][0] = Constants.GameShapes.J;
        }
        else if(mRotationCycle == 2)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(2, 3);

            //|L|L|L|
            //|0|0|L|
            mPotentialBlockGrid[0][0] = Constants.GameShapes.J;
            mPotentialBlockGrid[0][1] = Constants.GameShapes.J;
            mPotentialBlockGrid[0][2] = Constants.GameShapes.J;
            mPotentialBlockGrid[1][2] = Constants.GameShapes.J;
        }
        else if (mRotationCycle == 3)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(3, 2);

            //|0|L|
            //|0|L|
            //|L|L|
            mPotentialBlockGrid[0][1] = Constants.GameShapes.J;
            mPotentialBlockGrid[1][1] = Constants.GameShapes.J;
            mPotentialBlockGrid[2][0] = Constants.GameShapes.J;
            mPotentialBlockGrid[2][1] = Constants.GameShapes.J;
        }

        if(mRotationCycle++ == 3)
            mRotationCycle = 0;
    }
}
