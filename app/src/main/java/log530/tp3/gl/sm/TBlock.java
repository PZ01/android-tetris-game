package log530.tp3.gl.sm;

import android.content.Context;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;

/**
 * Created by david on 7/10/15.
 */
public class TBlock extends Block {

    private final static int T_CELL_WIDTH = 3;
    private final static int T_CELL_HEIGHT = 2;

    /**
     * Base Block constructor
     *
     * @param type        the underlying shape type(L-block, Square-block, etc...)
     * @param posX        the horizontal axis spawning position
     */
    public TBlock(Context ctx, Constants.GameShapes type, int posX) {
        super(ctx, type, posX, T_CELL_WIDTH, T_CELL_HEIGHT);

        //[row][col]
        mBlockGrid[0][0] = Constants.GameShapes.T;
        mBlockGrid[0][1] = Constants.GameShapes.T;
        mBlockGrid[0][2] = Constants.GameShapes.T;

        mBlockGrid[1][0] = Constants.GameShapes.EMPTY;
        mBlockGrid[1][1] = Constants.GameShapes.T;
        mBlockGrid[1][2] = Constants.GameShapes.EMPTY;
    }

    @Override
    public void potentialRotate() {
        if(mRotationCycle == 0)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(3, 2);

            //|0|T|
            //|T|T|
            //|0|T|
            mPotentialBlockGrid[0][1] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][0] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][1] =Constants.GameShapes.T;
            mPotentialBlockGrid[2][1] =Constants.GameShapes.T;


        }
        else if(mRotationCycle == 1)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(2, 3);

            //|0|T|0|
            //|T|T|T|
            mPotentialBlockGrid[0][1] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][0] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][1] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][2] =Constants.GameShapes.T;


        }
        else if(mRotationCycle == 2)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(3, 2);

            //|T|0|
            //|T|T|
            //|T|0|
            mPotentialBlockGrid[0][0] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][0] =Constants.GameShapes.T;
            mPotentialBlockGrid[2][0] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][1] =Constants.GameShapes.T;

        }
        else if (mRotationCycle == 3)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(2, 3);

            //|T|T|T|
            //|0|T|0|
            mPotentialBlockGrid[0][0] =Constants.GameShapes.T;
            mPotentialBlockGrid[0][1] =Constants.GameShapes.T;
            mPotentialBlockGrid[0][2] =Constants.GameShapes.T;
            mPotentialBlockGrid[1][1] =Constants.GameShapes.T;
        }

        if(mRotationCycle++ == 3)
            mRotationCycle = 0;
    }
}
