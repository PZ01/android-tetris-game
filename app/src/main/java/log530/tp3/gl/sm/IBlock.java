package log530.tp3.gl.sm;

import android.content.Context;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;

/**
 * Created by root on 7/10/15.
 */
public class IBlock  extends Block {

    private final static int I_CELL_WIDTH = 1;
    private final static int I_CELL_HEIGHT = 4;

    /**
     * Base Block constructor
     *
     * @param type        the underlying shape type(L-block, Square-block, etc...)
     * @param posX        the horizontal axis spawning position
     */
    public IBlock(Context ctx, Constants.GameShapes type, int posX) {
        super(ctx, type, posX, I_CELL_WIDTH, I_CELL_HEIGHT);

        //row col
        mBlockGrid[0][0] = Constants.GameShapes.I;
        mBlockGrid[1][0] = Constants.GameShapes.I;
        mBlockGrid[2][0] = Constants.GameShapes.I;
        mBlockGrid[3][0] = Constants.GameShapes.I;
    }

    @Override
    public void potentialRotate() {
        if(mRotationCycle == 0)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(1, 4);

            //|I|I|I|I|
            mPotentialBlockGrid[0][0] =  Constants.GameShapes.I;
            mPotentialBlockGrid[0][1] =  Constants.GameShapes.I;
            mPotentialBlockGrid[0][2] =  Constants.GameShapes.I;
            mPotentialBlockGrid[0][3] =  Constants.GameShapes.I;
        }
        else if(mRotationCycle == 1)
        {
            clearGrid(mPotentialBlockGrid);
            mPotentialBlockGrid = create2dArray(4, 1);

            //|I|
            //|I|
            //|I|
            //|I|
            mPotentialBlockGrid[0][0] =  Constants.GameShapes.I;
            mPotentialBlockGrid[1][0] =  Constants.GameShapes.I;
            mPotentialBlockGrid[2][0] =  Constants.GameShapes.I;
            mPotentialBlockGrid[3][0] =  Constants.GameShapes.I;
        }


        if(mRotationCycle++ == 1)
            mRotationCycle = 0;
    }
}
