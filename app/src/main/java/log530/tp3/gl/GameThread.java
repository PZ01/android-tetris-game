package log530.tp3.gl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.View;

import log530.tp3.GameActivity;
import log530.tp3.R;
import log530.tp3.ui.GameView;
import log530.tp3.ui.GridView;
import log530.tp3.util.PaintManager;
import log530.tp3.util.BlockFactory;
import log530.tp3.util.ScoreBoard;
import log530.tp3.util.SoundManager;
import log530.tp3.util.Stopwatch;

public class GameThread extends Thread {


    public final int MAX_FPS = 50;
    public final int MAX_FRAME_SKIPS = 5;
    public final int FRAME_PERIOD = 1000 / MAX_FPS;

    private enum GAME_STATE {
        READY,
        RUNNING,
        PAUSED,
        WON,
        LOST,
    };

    private Context mContext;
    private PaintManager paintManager;
    private Stopwatch mGameClock;
    private boolean mGameOver;
    private String mGameOverText;
    private int mMoveDownFrequency; // Expressed in milliseconds (drop/ms)
    private GridView mGridView;
    private Block mDroppingBlock;
    private GAME_STATE mCurrentState;
    private boolean mIsInForeground;
    private final Object mDrawLock;
    private SurfaceHolder mSurfaceHolder;
    private int mBgColor;
    private int mSurfaceWidth;
    private int mSurfaceHeight;
    private ScoreBoard mScoreBoard;
    /** Message handler used by thread to interact with UI in ui-thread*/
    private Handler mHandler;


    public GameThread(final SurfaceHolder surfaceHolder, final Context ctx, final AttributeSet attrs) {

        mContext = ctx;
        paintManager = PaintManager.getInstance(mContext.getResources());
        mGameClock = new Stopwatch();
        mGameOver = false;
        mGameOverText = mContext.getResources().getString(R.string.app_gameover);
        mGridView = new GridView(ctx, attrs);
        mDroppingBlock = BlockFactory.getInstance().CreateEmptyShape(mContext);
        mCurrentState = GAME_STATE.READY;
        mIsInForeground = true;
        mDrawLock = new Object();
        mSurfaceHolder = surfaceHolder;
        mBgColor = mContext.getResources().getColor(R.color.game_bg_color);
        mMoveDownFrequency = 200;
        mScoreBoard = new ScoreBoard(ctx, 10, 10);

        SoundManager.initSounds(this.mContext);
    }

    @Override
    public void run() {
        long beginTime = 0;       // the time when the cycle begun
        long timeDiff = 0;        // the time it took for the cycle to execute
        int sleepTime = 0;        // ms to sleep (<0 if we're behind)
        int framesSkipped = 0;    // number of frames being skipped

        while (mIsInForeground) {
            beginTime = SystemClock.currentThreadTimeMillis();
            framesSkipped = 0;

            Canvas canvas = null;

            try {
                canvas = mSurfaceHolder.lockCanvas();

                synchronized (mSurfaceHolder) {
                    if (mCurrentState == GAME_STATE.RUNNING) {
                        updateLogic();
                    }

                    synchronized (mDrawLock)
                    {
                        if (mIsInForeground)
                            draw(canvas);
                    }
                }
            } finally {
                if (canvas != null) {
                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }

            timeDiff = SystemClock.currentThreadTimeMillis() - beginTime;
            sleepTime = (int) (FRAME_PERIOD - timeDiff);

            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (final InterruptedException e) {
                    e.printStackTrace();
                }
            }

            while (sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS) {
                sleepTime += FRAME_PERIOD;
                framesSkipped++;
            }
        }
    }

    private void draw(Canvas canvas) {

        if (!mGameOver) {
            //Clear the screen
            canvas.drawColor(mBgColor);
            mGridView.draw(canvas);
            mDroppingBlock.draw(canvas, mGridView.bounds());
            mScoreBoard.draw(canvas);
        }
        else
        {
            //Draw background overlay
            canvas.drawPaint(paintManager.FillMattGrayLowOpacityPaint());

            Paint whitePaint = paintManager.FillWhiteText();
            //PZ::TODO Precompute this
            float textXPos = mSurfaceWidth / 2;
            canvas.drawText(mGameOverText, textXPos, mSurfaceHeight / 2, whitePaint);
        }
    }

    /**
     * Pauses the physics update & animation.
     */
    public void pause() {
        synchronized (mSurfaceHolder) {
            if (mCurrentState == GAME_STATE.RUNNING) {
                //setState(STATE_PAUSE);
            }
        }
    }


    private void updateLogic() {

        if (isGameOver())
        {
            Message msg = mHandler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putInt(GameActivity.VISIBILITY_KEY, View.GONE);
            msg.setData(bundle);
            mHandler.sendMessage(msg);
            return;
        }

        if(mDroppingBlock.isNull()) //No shape in the air, spawn one.
        {
            mDroppingBlock = BlockFactory.getInstance().CreateShape(mContext, mGridView.gridModel().getHorizontalCellCnt());
            mDroppingBlock.resize(mGridView.getCellWidthUI(), mGridView.getCellHeightUI());
            mGameClock.reset();
        }
        else
        {
            if(mGameClock.getElapsedTime().getElapsedRealtimeMillis() > mMoveDownFrequency)
            {
                boolean canMoveDown = mGridView.gridModel().attemptPeriodicShapeMoveDown(mDroppingBlock);
                if(!canMoveDown)
                {
                    //Once we put the shape down we,
                    //-Delete the falling shape since it's imprinted in the grid.
                    //-We possibly clear filled rows by the player.
                    //-We check for a loss.
                    //-Sound on hit.
                    //Deal with scoring logic
                    mDroppingBlock = BlockFactory.getInstance().CreateEmptyShape(mContext);
                    SoundManager.playSound(this.mContext, 2);;

                    mScoreBoard.blockDropped();

                    int numRowsCleared = 0;
                    boolean rowsWereCleared = mGridView.gridModel().possiblyClearRows(numRowsCleared);
                    if (rowsWereCleared)
                    {
                        SoundManager.playSound(this.mContext, 1);

                        mScoreBoard.clearedRows(numRowsCleared);
                    }

                    mScoreBoard.updateLevel();
                    increaseSpeed();
                    mGameOver = mGridView.gridModel().checkForLoss();

                }

                mGameClock.reset();
            }
        }

    }

    void increaseSpeed()
    {
        switch( mScoreBoard.getCurrLevel() )
        {
            case 2:
                mMoveDownFrequency = 150;
                break;
            case 3:
                mMoveDownFrequency = 100;
                break;
            case 4:
                mMoveDownFrequency = 50;
                break;
            case 5:
                mMoveDownFrequency = 10;
                break;
        }
    }

    synchronized public void moveBlock(Constants.PossibleMoves possibleMove)
    {
        mGridView.gridModel().attemptMoveShapeByKeys(mDroppingBlock, possibleMove);
    }

    synchronized public void rotateBlock()
    {
        mGridView.gridModel().attemptRotateShape(mDroppingBlock);
    }

    synchronized public void instantDropBlock()
    {
        mGridView.gridModel().attemptInstantDrop(mDroppingBlock);
    }

    public void setSurfaceSize(int width, int height)
    {
        mSurfaceWidth = width;
        mSurfaceHeight = height;
        mGridView.resize(mSurfaceWidth, mSurfaceHeight);
    }

    public boolean isInBackground()
    {
        return !mIsInForeground;
    }

    public void setInBackground(boolean isInBackground) {
        synchronized (mDrawLock)
        {
            mIsInForeground = !isInBackground;
        }
    }

    public boolean isGameOver()
    {
        return mGameOver;
    }

    public void setRunning()
    {
        mCurrentState = GAME_STATE.RUNNING;
    }

    public void setUIHandler(Handler handler)
    {
        mHandler = handler;
    }
}
