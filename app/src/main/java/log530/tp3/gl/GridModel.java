package log530.tp3.gl;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;

import java.util.Arrays;

import log530.tp3.R;

/**
 * This class represents game-logic of the grid. It holds a
 * collection of shapes.
 * <p/>
 * The logic handles collision detection and movement of the shapes.
 * GridModel is intended to have no reference to UI elements or measurements.
 * To display the Grid, the GridView class is responsible for rendering.
 */
public class GridModel {

    private int mGrid[][];
    private final int mHorizontalCellCnt;
    private final int mVerticalCellCnt;

    private Bitmap mSrcBitmap;
    private Bitmap mBlockBitmap;
    private final int mBlockWidthPx;
    private final int mBlockHeightPx;

    public GridModel(Context ctx, AttributeSet attrs) {

        // Unpack xml attributes
        TypedArray typedArr = ctx.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.GameView,
                0, 0);

        try {
            mHorizontalCellCnt = typedArr.getInt(R.styleable.GameView_gridHorizontalCellCnt, 0);
            mVerticalCellCnt = typedArr.getInt(R.styleable.GameView_gridVerticalCellCnt, 0);
        } finally {
            typedArr.recycle();
        }

        mGrid = new int[mVerticalCellCnt][mHorizontalCellCnt];

        //temporary
        mSrcBitmap = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.blocks);
        mBlockWidthPx = 40;
        mBlockHeightPx = mSrcBitmap.getHeight();
    }

    public int getHorizontalCellCnt() {
        return mHorizontalCellCnt;
    }

    public int getVerticalCellCnt() {
        return mVerticalCellCnt;
    }

    public Bitmap colorByType(Constants.GameShapes gameShape) {
        //PZ::TODO Substract by one because shapes start at one since zero is empty
        //I don't like this personally
        int posXPx = mBlockWidthPx * (gameShape.val() - 1);
        int posYPx = 0;

        return Bitmap.createBitmap(mSrcBitmap, posXPx, posYPx, mBlockWidthPx, mBlockHeightPx);
    }

    boolean attemptPeriodicShapeMoveDown(Block block) {
        int blockHeight = block.shapeHeight();
        int blockWidth = block.shapeWidth();
        //Attempt at pushing the block down
        block.potentialMove(Constants.PossibleMoves.DOWN, mHorizontalCellCnt);

        for (int row = 0; row < blockHeight; row++) {
            for (int col = 0; col < blockWidth; col++) {
                if (block.blockAt(row, col) != Constants.GameShapes.EMPTY) {
                    int trueRow = row + block.potentialY();
                    int trueCol = col + block.potentialX();
                    int collisionRow = block.row();
                    int collisionCol = block.col();

                    if (trueRow >= mGrid.length) {
                        copyShapeToGrid(block, collisionRow, collisionCol);
                        block.potentialMove(Constants.PossibleMoves.UP, mHorizontalCellCnt);

                        return false;
                    }

                    if (trueCol == 10)
                    {
                        int a = 0;
                        a++;
                    }
                    if (mGrid[trueRow][trueCol] != Constants.GameShapes.EMPTY.val()) {
                        copyShapeToGrid(block, collisionRow, collisionCol);
                        block.potentialMove(Constants.PossibleMoves.UP, mHorizontalCellCnt);

                        return false;
                    }
                }
            }
        }

        //If we got here, the block has not encountered any collision
        //we change it's position.
        block.move(Constants.PossibleMoves.DOWN, mHorizontalCellCnt);
        return true;
    }

    void copyShapeToGrid(Block block, final int blockTopRow, int blockTopCol) {
        int blockHeight = block.shapeHeight();
        int blockWidth = block.shapeWidth();

        for (int row = 0; row < blockHeight; row++) {
            for (int col = 0; col < blockWidth; col++) {
                if (block.blockAt(row, col) != Constants.GameShapes.EMPTY) {
                    int trueRow = blockTopRow + row;
                    int trueCol = blockTopCol + col;
                    mGrid[trueRow][trueCol] = block.type().val();
                }
            }
        }
    }

    boolean attemptMoveShapeByKeys(Block block, Constants.PossibleMoves possibleMove) {
        int blockHeight = block.shapeHeight();
        int blockWidth = block.shapeWidth();
        block.potentialMove(possibleMove, mHorizontalCellCnt);

        for (int row = 0; row < blockHeight; row++) {
            for (int col = 0; col < blockWidth; col++) {
                if (block.blockAt(row, col) != Constants.GameShapes.EMPTY) {
                    int trueRow = row + block.potentialY();
                    int trueCol = col + block.potentialX();

                    if (trueCol < 0 || trueCol >= mHorizontalCellCnt || trueRow >= mGrid.length) {
                        //Potential val is out of bounds, we must reverse it.
                        if (possibleMove == Constants.PossibleMoves.DOWN) {
                            block.potentialMove(Constants.PossibleMoves.UP, mHorizontalCellCnt);
                            return false;
                        }
                        block.potentialMove(possibleMove.reverse(), mHorizontalCellCnt);
                        return false;
                    }

                    if (mGrid[trueRow][trueCol] != Constants.GameShapes.EMPTY.val()) {
                        if (possibleMove == Constants.PossibleMoves.DOWN) {
                            block.potentialMove(Constants.PossibleMoves.UP, mHorizontalCellCnt);
                            return false;
                        }
                        block.potentialMove(possibleMove.reverse(), mHorizontalCellCnt);
                        return false;
                    }
                }
            }
        }

        block.move(possibleMove, mHorizontalCellCnt);
        return true;
    }

    boolean attemptRotateShape(Block block) {
        //If we are on the far edge from either 8 or 9 we can't rotate
        if (block.col() == (mHorizontalCellCnt - 1) || block.col() == (mHorizontalCellCnt - 2))
            return false;

        int blockHeight = block.shapeHeight();
        int blockWidth = block.shapeWidth();
        block.potentialRotate();

        for (int row = 0; row < blockHeight; row++) {
            for (int col = 0; col < blockWidth; col++) {
                int trueRow = row + block.potentialY();
                int trueCol = col + block.potentialX();

                if (trueCol < 0 || trueCol >= mHorizontalCellCnt || trueRow >= mGrid.length) {
                    block.decreaseRotationCycle();
                    return false;
                }

                if (mGrid[trueRow][trueCol] != Constants.GameShapes.EMPTY.val()) {
                    block.decreaseRotationCycle();
                    return false;
                }
            }
        }

        block.rotate();
        return true;
    }

    void attemptInstantDrop(Block block) {
        // Terrible performance, needs revisiting.
        while (attemptPeriodicShapeMoveDown(block)) {
        }
    }


    boolean checkForLoss() {
        boolean blockAtTop = false;
        for (int col = 0; col < mHorizontalCellCnt; ++col)
            if (mGrid[0][col] != Constants.GameShapes.EMPTY.val())
                blockAtTop = true;

        return blockAtTop;
    }

    boolean possiblyClearRows(Integer rowsCleared) {
        boolean rowsHaveBeenCleared = false;
        int row = 0;
        for (; row < mVerticalCellCnt; row++) {
            if (rowIsFilled(row)) {
                copyLinesFromAbove(row);
                rowsHaveBeenCleared = true;
            }
        }

        rowsCleared = row;
        return rowsHaveBeenCleared;
    }

    void copyLinesFromAbove(int rowDst) {
        for (; rowDst > 0; --rowDst)
            mGrid[rowDst] = mGrid[rowDst - 1];

        for (int col = 0; col < mHorizontalCellCnt; ++col)
            mGrid[rowDst][col] = Constants.GameShapes.EMPTY.val(); //PZ::TODO Make sure this is safe
    }

    boolean rowIsFilled(int pRow) {
        for (int col = 0; col < mHorizontalCellCnt; col++) {
            if (mGrid[pRow][col] == Constants.GameShapes.EMPTY.val()) {
                return false;
            }
        }

        return true;
    }


    @Deprecated
    public int[][] grid() {
        return mGrid;
    }

    void printGrid() {
        System.out.println("---------------------");
        for (int[] row : mGrid) {
            for (int i : row) {
                System.out.print(i);
                System.out.print("\t");
            }
            System.out.println();
        }
    }

}
