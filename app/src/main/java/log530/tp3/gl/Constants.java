package log530.tp3.gl;

/**
 * Created by pz on 30/06/15.
 */
public class Constants {

    public enum GameShapes {
        EMPTY(0),
        I(1),
        J(2),
        L(3),
        O(4),
        S(5),
        T(6),
        Z(7);

        private int mGameShape;

        GameShapes(int gameShape) {
            mGameShape = gameShape;
        }

        public int val()
        {
           return mGameShape;
        }
    }

    public enum PossibleMoves {
        LEFT(-1),
        RIGHT(1),
        DOWN(0),
        UP(2);

        private int mPossibleMove;

        PossibleMoves(int possibleMove) {
            mPossibleMove = possibleMove;
        }

        public PossibleMoves reverse() {
            switch (mPossibleMove * -1) {
                case -1:
                    return LEFT;
                case 1:
                    return DOWN;
                case 0:
                    return RIGHT;
                case 2:
                    return UP;
            }

            return null;
        }

        public int val()
        {
            return mPossibleMove;
        }
    }

    public static final int FONT_SIZE = 50;
    public static final int SCORE_TITLE_DROP = 10;
    public static final int SCORE_TITLE_CLEAR = 100;

}
