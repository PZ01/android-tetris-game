package log530.tp3.util;

import android.annotation.TargetApi;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.AudioManager;
import android.content.Context;
import android.os.Build;

import log530.tp3.R;


/**
 * Created by david on 05/07/15.
 *
 * Code is taken from
 * http://blog.ashwanik.in/2014/12/play-sounds-in-android-application.html
 */
public class SoundManager {

    private static MediaPlayer mediaPlayer;

    public static final int S1 = R.raw.hit;
    public static final int S2 = R.raw.old;
    public static final int S3 = R.raw.clear;

    private static SoundPool soundPool;

    /**
     * Populate the SoundPool
     */
    public static void initSounds(Context context) {

        soundPool = buildSoundPool();

        soundPool.load(context, R.raw.clear, 1);
        soundPool.load(context, R.raw.hit, 2);
        soundPool.load(context, R.raw.old, 3);

        mediaPlayer = MediaPlayer.create(context, R.raw.background);
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static SoundPool buildSoundPool() {
        final int max_streams = 3;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(max_streams)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(max_streams, AudioManager.STREAM_MUSIC, 0);
        }
        return soundPool;
    }

    public static void initBackgroundMusic(Context context){
         mediaPlayer = MediaPlayer.create(context, R.raw.background);
         mediaPlayer.setLooping(true);
         mediaPlayer.start();
    }

    /**
     * Play a given sound in the soundPool
     */
    public static void playSound(Context context, int soundID) {
        if (soundPool == null) {
            initSounds(context);
        }
        float volume = 0.5f; //0.0 to 1.0

        //play(int soundID, float leftVolume, float rightVolume, int priority, int loop, float rate)
        soundPool.play(soundID, volume, volume, 1, 0, 1);
    }
}
