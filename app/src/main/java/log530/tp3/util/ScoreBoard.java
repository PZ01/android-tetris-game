package log530.tp3.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import log530.tp3.gl.Constants;

public class ScoreBoard {
    public ScoreBoard(final Context ctx, int pLeftCornerPosX, int pLeftCornerPosY) {

        _posX = pLeftCornerPosX;
        _posY = pLeftCornerPosY + Constants.FONT_SIZE;
    }

    public void blockDropped() {
        _score += Constants.SCORE_TITLE_DROP;
    }

    public void clearedRows(int pNumClearedRows) {
        _score += Constants.SCORE_TITLE_CLEAR * pNumClearedRows;
    }

    public void updateLevel() {
        if (_score < 2000) {
            _level = 1;
        } else if (_score < 5000) {
            _level = 2;
        } else if(_score < 10000) {
            _level = 3;
        } else if(_score < 20000) {
            _level = 4;
        } else {
            _level = 5;
        }
    }

    private int _score = 0;
    private int _level = 1;
    private int _posX;
    private int _posY;

    public void draw(Canvas canvas) {
        Paint scorePaint = new Paint();
        scorePaint.setStyle(Paint.Style.FILL);
        scorePaint.setColor(Color.argb(255, 233, 210, 213));
        scorePaint.setTextSize(Constants.FONT_SIZE);

        canvas.drawText(String.format("Score : %d", _score), _posX + 15, _posY + 8, scorePaint);
        canvas.drawText(String.format("Level : %d", _level), _posX + 15, _posY + Constants.FONT_SIZE + 8, scorePaint);
    }

    public int getCurrLevel() {
        return _level;
    }
}
