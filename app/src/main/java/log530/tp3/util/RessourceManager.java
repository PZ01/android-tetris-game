package log530.tp3.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import log530.tp3.R;

/**
 * Created by pz on 11/07/15.
 */
public class RessourceManager {

    private static RessourceManager instance = null;
    private Bitmap mStrippedBg;
    private Bitmap mBlockSprite;

	protected RessourceManager(Context ctx) {

        mStrippedBg = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.stripesbg);
        mBlockSprite = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.blocks);
	}

	public static RessourceManager getInstance(Context ctx) {

        if (instance == null) {
            instance = new RessourceManager(ctx);
        }

        return instance;
    }

    public Bitmap StrippedBg()
    {
        return mStrippedBg;
    }

    public Bitmap BlockSprite()
    {
        return mBlockSprite;
    }
}
