package log530.tp3.util;

import java.util.Random;

/**
 * Created by pz on 08/07/15.
 */
public class Utility {

    private Random mRand;
    private static final Utility instance = new Utility();

	protected Utility() {
        mRand = new Random();
	}

	public static Utility getInstance() {
        return instance;
    }

    public int randInRange(int min, int max)
    {
        int range = Math.abs(max - min) + 1;
        return mRand.nextInt(range) + min;
    }
}
