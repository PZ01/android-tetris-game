package log530.tp3.util;

import android.content.Context;

import log530.tp3.gl.Block;
import log530.tp3.gl.Constants;
import log530.tp3.gl.sm.*;

public class BlockFactory {

   	private static final BlockFactory instance = new BlockFactory();

	protected BlockFactory() {
	}

	public static BlockFactory getInstance() {
		return instance;
	}

    private Constants.GameShapes generateShapeID()
    {
		int shapeIndex = Utility.getInstance().randInRange(1, 7);
		return Constants.GameShapes.values()[shapeIndex];
    }

    private int generateHorizontalSpawnPosition(int horizontalMax)
    {
		return Utility.getInstance().randInRange(0, horizontalMax);
    }

    public Block CreateShape(Context ctx, int horizontalMax)
    {
        return CreateShape(ctx, generateShapeID(), generateHorizontalSpawnPosition(horizontalMax));
    }

    public Block CreateEmptyShape(Context ctx)
    {
        return new EmptyBlock(ctx);
    }

	public Block CreateShape(Context ctx, Constants.GameShapes gameShape, int horizontalSpawnPos)
	{
		switch (gameShape)
		{
			case EMPTY:
				break;
			case I:
				return new IBlock(ctx, gameShape, horizontalSpawnPos);
			case J:
				return new JBlock(ctx, gameShape, horizontalSpawnPos);
			case L:
				return new LBlock(ctx, gameShape, horizontalSpawnPos);
			case O:
				return new OBlock(ctx, gameShape, horizontalSpawnPos);
			case S:
				return new SBlock(ctx, gameShape, horizontalSpawnPos);
			case T:
				return new TBlock(ctx, gameShape, horizontalSpawnPos);
			case Z:
				return new ZBlock(ctx, gameShape, horizontalSpawnPos);
		}

		return null;
	}
}
