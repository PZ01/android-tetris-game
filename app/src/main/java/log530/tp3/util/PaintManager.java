package log530.tp3.util;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;

import log530.tp3.R;

public class PaintManager {

    private static PaintManager instance = null;
    private static Paint mGenericPaint;
    private static Paint mDebugPaint;
    private static Paint mLargeWhiteText;
    private static Paint mFillYellow;
    private static Paint mThickBlackStrokePaint;
    private static Paint mThickGrayStrokePaint;
    private static Paint mFillMattGrayLowOpacityPaint;


    protected PaintManager(Resources res) {

        mGenericPaint = new Paint();
        mGenericPaint.setAntiAlias(true);
        mGenericPaint.setStyle(Paint.Style.FILL);

        mDebugPaint = new Paint();
        mDebugPaint.setStyle(Paint.Style.STROKE);
        mDebugPaint.setStrokeWidth(10);
        mDebugPaint.setColor(res.getColor(R.color.game_dbg_color));

        mLargeWhiteText = new Paint();
        mLargeWhiteText.setStyle(Paint.Style.FILL);
        mLargeWhiteText.setColor(Color.WHITE);
        mLargeWhiteText.setTextAlign(Paint.Align.CENTER);
        mLargeWhiteText.setTextSize(72);

        mFillYellow = new Paint();
        mFillYellow.setStyle(Paint.Style.FILL);
        mFillYellow.setColor(Color.YELLOW);

        mThickBlackStrokePaint = new Paint();
        mThickBlackStrokePaint.setStyle(Paint.Style.STROKE);
        mThickBlackStrokePaint.setStrokeWidth(1);
        mThickBlackStrokePaint.setColor(Color.BLACK);

        mThickGrayStrokePaint = new Paint();
        mThickGrayStrokePaint.setStyle(Paint.Style.STROKE);
        mThickGrayStrokePaint.setStrokeWidth(5);
        mThickGrayStrokePaint.setColor(Color.GRAY);

        mFillMattGrayLowOpacityPaint = new Paint();
        mFillMattGrayLowOpacityPaint.setStyle(Paint.Style.FILL);
        mFillMattGrayLowOpacityPaint.setColor(res.getColor(R.color.game_bg_overlay_color));
        mFillMattGrayLowOpacityPaint.setAlpha(30);
    }

    public static PaintManager getInstance(Resources res) {
        if (instance == null) {
            instance = new PaintManager(res);
        }
        return instance;
    }

    public static Paint GenericPaint() {
        return mGenericPaint;
    }

    public Paint Debug()
    {
        return mDebugPaint;
    }

    public Paint FillWhiteText()
    {
        return mLargeWhiteText;
    }

    public static Paint FillYellow() {
        return mFillYellow;
    }

    public Paint BlackStroke()
    {
        return mThickBlackStrokePaint;
    }

    public Paint ThickGrayStroke()
    {
        return mThickGrayStrokePaint;
    }

    public Paint FillMattGrayLowOpacityPaint()
    {
        return mFillMattGrayLowOpacityPaint;
    }
}
