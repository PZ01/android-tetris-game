import org.junit.Test;
import java.util.regex.Pattern;

import log530.tp3.util.Utility;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UtilityTest {

    @Test
    public void randInRange_FixedRange_ReturnsInRange() {

        boolean encounteredError = false;
        int iterations = 1000;
        int min = 0;
        int max = 20;

        for (int i = 0 ; i < iterations ; i++)
        {
            int result = Utility.getInstance().randInRange(min, max);
            if(result > max || result < min)
            {
                encounteredError = true;
                break;
            }
        }

        assertFalse(encounteredError);
    }
}
